import pylab as pl
from neuman import *


# m^2/s
D = 1.5e-9

# m
R1 = 1e-6
R2 = 2.5e-6
R3 = 5e-6
R4 = 100e-6

# s
taus = np.linspace(1e-3,100e-3,100)

# T/m
G = 0.3



lnS_R1 = []
lnS_R2 = []
lnS_R3 = []
lnS_R4 = []

long_time_limit_R1 = []
long_time_limit_R2 = []
long_time_limit_R3 = []
long_time_limit_R4 = []

lnS_R1_list = []
lnS_R2_list = []
lnS_R3_list = []
lnS_R4_list = []

short_time_limit = []


for tau in taus:
	print(tau)
	tmp = neuman_cylinder_perp_ln(D, R1, tau, G, m_max=10)
	lnS_R1.append(tmp)
	tmp = neuman_cylinder_perp_ln(D, R2, tau, G, m_max=10)
	lnS_R2.append(tmp)
	tmp = neuman_cylinder_perp_ln(D, R3, tau, G, m_max=10)
	lnS_R3.append(tmp)
	tmp = neuman_cylinder_perp_ln(D, R4, tau, G, m_max=10)
	lnS_R4.append(tmp)

	tmp = neuman_cylinder_perp_ln_all(D, R1, tau, G, m_max=10)
	lnS_R1_list.append(tmp)
	tmp = neuman_cylinder_perp_ln_all(D, R2, tau, G, m_max=10)
	lnS_R2_list.append(tmp)
	tmp = neuman_cylinder_perp_ln_all(D, R3, tau, G, m_max=10)
	lnS_R3_list.append(tmp)
	tmp = neuman_cylinder_perp_ln_all(D, R4, tau, G, m_max=10)
	lnS_R4_list.append(tmp)

	short_time_limit.append(neuman_cylinder_short_time_limit_ln(D, tau, G))

	long_time_limit_R1.append(neuman_cylinder_long_time_limit_ln(D, R1, tau, G))
	long_time_limit_R2.append(neuman_cylinder_long_time_limit_ln(D, R2, tau, G))
	long_time_limit_R3.append(neuman_cylinder_long_time_limit_ln(D, R3, tau, G))
	long_time_limit_R4.append(neuman_cylinder_long_time_limit_ln(D, R4, tau, G))


lnS_R1_list = np.array(lnS_R1_list)
lnS_R2_list = np.array(lnS_R2_list)
lnS_R3_list = np.array(lnS_R3_list)
lnS_R4_list = np.array(lnS_R4_list)



pl.figure()
pl.plot(np.array(short_time_limit) / -D / 10**6, np.exp(short_time_limit))
pl.xlabel('bvalue (s/mm^2)', size=20)
pl.ylabel('Free Diff signal decay', size=20)
pl.title('Carr type sequence, short time limit Neuman cylinder')
pl.show()



# pl.figure()
# pl.plot(taus, lnS_R1, label='ln R1 all')
# pl.plot(taus, lnS_R2, label='ln R2 all')
# pl.plot(taus, lnS_R3, label='ln R3 all')
# pl.legend()



# pl.figure()
# for i in range(lnS_R1_list.shape[1]):
# 	pl.plot(2*taus, lnS_R1_list[:,i], label='{} terms'.format(i+1))
# pl.title('ln R1')
# pl.legend()

# pl.figure()
# for i in range(lnS_R1_list.shape[1]):
# 	pl.plot(2*taus, lnS_R2_list[:,i], label='{} terms'.format(i+1))
# pl.title('ln R2')
# pl.legend()

# pl.figure()
# for i in range(lnS_R1_list.shape[1]):
# 	pl.plot(2*taus, lnS_R3_list[:,i], label='{} terms'.format(i+1))
# pl.title('ln R3')
# pl.legend()

# pl.show()



# pl.figure()
# for i in range(lnS_R1_list.shape[1]):
# 	pl.plot(2*taus, np.exp(lnS_R1_list[:,i]), label='{} terms'.format(i+1))
# pl.title('R1')
# pl.legend()

# pl.figure()
# for i in range(lnS_R1_list.shape[1]):
# 	pl.plot(2*taus, np.exp(lnS_R2_list[:,i]), label='{} terms'.format(i+1))
# pl.title('R2')
# pl.legend()

# pl.figure()
# for i in range(lnS_R1_list.shape[1]):
# 	pl.plot(2*taus, np.exp(lnS_R3_list[:,i]), label='{} terms'.format(i+1))
# pl.title('R3')
# pl.legend()

# pl.show()




# pl.figure()
# for i in range(lnS_R1_list.shape[1]):
# 	pl.plot(2*taus, np.exp(lnS_R1_list[:,i])-np.exp(short_time_limit), label='{} terms'.format(i+1))
# pl.title('R1')
# pl.legend()

# pl.figure()
# for i in range(lnS_R1_list.shape[1]):
# 	pl.plot(2*taus, np.exp(lnS_R2_list[:,i])-np.exp(short_time_limit), label='{} terms'.format(i+1))
# pl.title('R2')
# pl.legend()

# pl.figure()
# for i in range(lnS_R1_list.shape[1]):
# 	pl.plot(2*taus, np.exp(lnS_R3_list[:,i])-np.exp(short_time_limit), label='{} terms'.format(i+1))
# pl.title('R3')
# pl.legend()

# pl.show()




pl.figure()
pl.plot(2*taus, np.exp(lnS_R1_list[:,-1]), label='R1 = {}'.format(R1))
pl.plot(2*taus, np.exp(lnS_R2_list[:,-1]), label='R2 = {}'.format(R2))
pl.plot(2*taus, np.exp(lnS_R3_list[:,-1]), label='R3 = {}'.format(R3))
pl.plot(2*taus, np.exp(lnS_R4_list[:,-1]), label='R4 = {}'.format(R4))
pl.plot(2*taus, np.exp(short_time_limit), label='Free diff')

excl_R1 = ~(np.array(long_time_limit_R1)>0)
excl_R2 = ~(np.array(long_time_limit_R2)>0)
excl_R3 = ~(np.array(long_time_limit_R3)>0)
excl_R4 = ~(np.array(long_time_limit_R4)>0)
pl.plot(2*taus[excl_R1], np.exp(long_time_limit_R1)[excl_R1], label='long lim R1')
pl.plot(2*taus[excl_R2], np.exp(long_time_limit_R2)[excl_R2], label='long lim R2')
pl.plot(2*taus[excl_R3], np.exp(long_time_limit_R3)[excl_R3], label='long lim R3')
pl.plot(2*taus[excl_R4], np.exp(long_time_limit_R4)[excl_R4], label='long lim R4')
pl.legend()
pl.show()

