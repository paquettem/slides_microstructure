import numpy as np
import pylab as pl

from matplotlib import rc
rc('text', usetex=True)

# m
diffusivity = 2.0e-9

# s
diff_time = 80.0e-3
t_step = 10.0e-6
N_init = 10000
N_particule = N_init



displacement_1 = np.load('/home/raid2/paquette/Documents/presentation/restricted_diffusion/code/displacement_D2p0_R5p0.npy')
times_1 = np.load('/home/raid2/paquette/Documents/presentation/restricted_diffusion/code/times_D2p0_R5p0.npy')
cyl_radius_1 = 5.0e-6
msd_1 = (1e12)*(displacement_1**2).mean(axis=1)
limit_1 = 0.5*((1e6)*cyl_radius_1)**2


displacement_2 = np.load('/home/raid2/paquette/Documents/presentation/restricted_diffusion/code/displacement_D2p0_R2p5.npy')
times_2 = np.load('/home/raid2/paquette/Documents/presentation/restricted_diffusion/code/times_D2p0_R2p5.npy')
cyl_radius_2 = 2.5e-6
msd_2 = (1e12)*(displacement_2**2).mean(axis=1)
limit_2 = 0.5*((1e6)*cyl_radius_2)**2

displacement_3 = np.load('/home/raid2/paquette/Documents/presentation/restricted_diffusion/code/displacement_D2p0_R1p0.npy')
times_3 = np.load('/home/raid2/paquette/Documents/presentation/restricted_diffusion/code/times_D2p0_R1p0.npy')
cyl_radius_3 = 1.0e-6
msd_3 = (1e12)*(displacement_3**2).mean(axis=1)
limit_3 = 0.5*((1e6)*cyl_radius_3)**2

displacement_4 = np.load('/home/raid2/paquette/Documents/presentation/restricted_diffusion/code/displacement_D2p0_R0p5.npy')
times_4 = np.load('/home/raid2/paquette/Documents/presentation/restricted_diffusion/code/times_D2p0_R0p5.npy')
cyl_radius_4 = 0.5e-6
msd_4 = (1e12)*(displacement_4**2).mean(axis=1)
limit_4 = 0.5*((1e6)*cyl_radius_4)**2




unrestricted = (1e12)*2*diffusivity*times_1[1:]


c1 = (1,0,0)
c2 = (0,1,0)
c3 = (0,0,1)
c4 = (1,0,1)
# c5 = (1,1,0)
# c6 = (0,1,1)
c7 = (0,0,0)



tl = times_1.shape[0]//80
# tl = times_1.shape[0]//20
# tl = times_1.shape[0]//10
# tl = times_1.shape[0]//5
# tl = times_1.shape[0]

label_size = 16
pl.rcParams['xtick.labelsize'] = label_size
pl.rcParams['ytick.labelsize'] = label_size

# pl.figure()
# pl.axhline(limit_1, c=c1, label=r'time $\rightarrow \infty$, R = {:.2f} $\mu$m'.format((1e6)*cyl_radius_1))
# pl.axhline(limit_2, c=c2, label=r'time $\rightarrow \infty$, R = {:.2f} $\mu$m'.format((1e6)*cyl_radius_2))
# pl.axhline(limit_3, c=c3, label=r'time $\rightarrow \infty$, R = {:.2f} $\mu$m'.format((1e6)*cyl_radius_3))
# pl.axhline(limit_4, c=c4, label=r'time $\rightarrow \infty$, R = {:.2f} $\mu$m'.format((1e6)*cyl_radius_4))
# # 1e3 ms = 1 s,          (1e6 um)^2 = 1 m^2,     1e6 um = 1 m
# pl.plot((1e3)*times_1[1:tl], msd_1[:tl-1], c=c1, label=r'MC, R = {:.2f} $\mu$m'.format((1e6)*cyl_radius_1))
# pl.plot((1e3)*times_1[1:tl], msd_2[:tl-1], c=c2, label=r'MC, R = {:.2f} $\mu$m'.format((1e6)*cyl_radius_2))
# pl.plot((1e3)*times_1[1:tl], msd_3[:tl-1], c=c3, label=r'MC, R = {:.2f} $\mu$m'.format((1e6)*cyl_radius_3))
# pl.plot((1e3)*times_1[1:tl], msd_4[:tl-1], c=c4, label=r'MC, R = {:.2f} $\mu$m'.format((1e6)*cyl_radius_4))
# pl.plot((1e3)*times_1[1:tl], unrestricted[:tl-1], c=c7, label='Free')


# pl.xlabel('Times (ms)', size=20)
# pl.ylabel(r'Mean Squared Displacement ($\mu$m$^2$)', size=20)
# pl.legend(fontsize=20)
# pl.title('MSD from 2D Montecarlo simulation (N = {} , step = {:.2e} ms)'.format(N_particule, (1e3)*t_step), size=20)
# pl.ylim(bottom=0, top=msd_1[:tl-1].max()*1.2)
# # pl.tight_layout()
# pl.show()

pl.figure()
pl.plot((1e3)*times_1[1:tl], unrestricted[:tl-1], c=c7, label='Free')
# 1e3 ms = 1 s,          (1e6 um)^2 = 1 m^2,     1e6 um = 1 m
pl.plot((1e3)*times_1[1:tl], msd_1[:tl-1], c=c1, label=r'd = {:.0f} $\mu$m'.format((1e6)*cyl_radius_1*2))
# pl.plot((1e3)*times_1[1:tl], msd_1[:tl-1], c=c1, label=r'MC, d = {:.0f} $\mu$m'.format((1e6)*cyl_radius_1*2))
pl.axhline(limit_1, c=c1)
# pl.axhline(limit_1, c=c1, label=r'time $\rightarrow \infty$, d = {:.0f} $\mu$m'.format((1e6)*cyl_radius_1*2))j
pl.plot((1e3)*times_1[1:tl], msd_2[:tl-1], c=c2, label=r'd = {:.0f} $\mu$m'.format((1e6)*cyl_radius_2*2))
# pl.plot((1e3)*times_1[1:tl], msd_2[:tl-1], c=c2, label=r'MC, d = {:.0f} $\mu$m'.format((1e6)*cyl_radius_2*2))
pl.axhline(limit_2, c=c2)
# pl.axhline(limit_2, c=c2, label=r'time $\rightarrow \infty$, d = {:.0f} $\mu$m'.format((1e6)*cyl_radius_2*2))
pl.plot((1e3)*times_1[1:tl], msd_3[:tl-1], c=c3, label=r'd = {:.0f} $\mu$m'.format((1e6)*cyl_radius_3*2))
# pl.plot((1e3)*times_1[1:tl], msd_3[:tl-1], c=c3, label=r'MC, d = {:.0f} $\mu$m'.format((1e6)*cyl_radius_3*2))
pl.axhline(limit_3, c=c3)
# pl.axhline(limit_3, c=c3, label=r'time $\rightarrow \infty$, d = {:.0f} $\mu$m'.format((1e6)*cyl_radius_3*2))
pl.plot((1e3)*times_1[1:tl], msd_4[:tl-1], c=c4, label=r'd = {:.0f} $\mu$m'.format((1e6)*cyl_radius_4*2))
# pl.plot((1e3)*times_1[1:tl], msd_4[:tl-1], c=c4, label=r'MC, d = {:.0f} $\mu$m'.format((1e6)*cyl_radius_4*2))
pl.axhline(limit_4, c=c4)
# pl.axhline(limit_4, c=c4, label=r'time $\rightarrow \infty$, d = {:.0f} $\mu$m'.format((1e6)*cyl_radius_4*2))


pl.xlabel('Times (ms)', size=20)
pl.ylabel(r'Mean Squared Displacement ($\mu$m$^2$)', size=20)
pl.legend(fontsize=20)
pl.title('MSD from 2D Montecarlo simulation (N = {} , step = {:.2e} ms)'.format(N_particule, (1e3)*t_step), size=20)
pl.ylim(bottom=0, top=msd_1[:tl-1].max()*1.2)
# pl.tight_layout()
pl.show()





