import numpy as np
from scipy.special import jnp_zeros



#  T^-1 s^-1
gamma = 42.515e6 * 2*np.pi

def neuman_cylinder_perp_ln_list(D, R, tau, G, m_max=5):
	# returns the scaling factor and the list of each summation component for ln(M(2*tau,G)/M(0))
	# D:= free diffusivity in m^2 s^-1
	# R:= cylinder radius in m
	# tau:= half diffusion time in s (assumes NPA, otherwise Delta+delta?)
	# G:= gradient magnitude in T m^-1

	am_R = jnp_zeros(1,m_max)
	am = am_R / R
	am2 = am**2

	fac = -2*gamma**2*G**2/D

	comp = ((am**-4)/(am_R**2 - 1)) * (2*tau - (3-4*np.exp(-am2*D*tau)+np.exp(-am2*D*2*tau))/(am2*D))

	return fac, comp


def neuman_cylinder_perp_ln_all(D, R, tau, G, m_max=5):
	fac, comp = neuman_cylinder_perp_ln_list(D, R, tau, G, m_max)
	return fac*np.cumsum(comp)


def neuman_cylinder_perp_ln(D, R, tau, G, m_max=5):
	fac, comp = neuman_cylinder_perp_ln_list(D, R, tau, G, m_max)
	return fac*np.sum(comp)


def neuman_cylinder_short_time_limit_ln(D, tau, G):
	return (-2/3.)*gamma**2*G**2*D*tau**3


def neuman_cylinder_long_time_limit_ln(D, R, tau, G):
	return ((-7*(R**2*gamma*G)**2)/(96.*D)) * (2*tau - ((99*R**2)/(112.*D)))

