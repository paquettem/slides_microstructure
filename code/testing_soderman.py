import pylab as pl
from soderman import *

#  T^-1 s^-1
gamma = 42.515e6 * 2*np.pi

# m^2/s
D = 1.5e-9

# m
R1 = 1e-6
R2 = 2.5e-6
R3 = 5e-6
R4 = 100e-6

# s
DELTAs = np.linspace(15e-3,100e-3,50)
deltas = np.linspace(1e-3,10e-3,10)

# T/m
G = 0.3

m_max = 10
k_max = 5

signals = np.zeros((4, len(deltas), len(DELTAs), k_max-1))


for i_d, delta in enumerate(deltas):
	for i_D, DELTA in enumerate(DELTAs):

		signals[0, i_d, i_D] = soderman_cylinder_perp_all(D, R1, DELTA, delta, G, k_max, m_max)
		signals[1, i_d, i_D] = soderman_cylinder_perp_all(D, R2, DELTA, delta, G, k_max, m_max)
		signals[2, i_d, i_D] = soderman_cylinder_perp_all(D, R3, DELTA, delta, G, k_max, m_max)
		signals[3, i_d, i_D] = soderman_cylinder_perp_all(D, R4, DELTA, delta, G, k_max, m_max)




for i_d, delta in enumerate(deltas):
	pl.figure()
	pl.subplot(2,2,1)
	for i in range(3,k_max-1):
		pl.plot(DELTAs, signals[0, i_d, :, i], label='{} terms'.format(i+1))
	pl.title('R1, delta={}'.format(deltas[i_d]))
	# pl.figure()
	pl.subplot(2,2,2)
	for i in range(3,k_max-1):
		pl.plot(DELTAs, signals[1, i_d, :, i], label='{} terms'.format(i+1))
	pl.title('R2, delta={}'.format(deltas[i_d]))
	# pl.figure()
	pl.subplot(2,2,3)
	for i in range(3,k_max-1):
		pl.plot(DELTAs, signals[2, i_d, :, i], label='{} terms'.format(i+1))
	pl.title('R3, delta={}'.format(deltas[i_d]))
	pl.subplot(2,2,4)
	for i in range(3,k_max-1):
		pl.plot(DELTAs, signals[3, i_d, :, i], label='{} terms'.format(i+1))
	pl.title('R4, delta={}'.format(deltas[i_d]))
	pl.legend()

pl.show()












