import numpy as np
from scipy.special import jnp_zeros



#  T^-1 s^-1
gamma = 42.515e6 * 2*np.pi


def vangelderen_cylinder_perp_ln_list(D, R, DELTA, delta, G, m_max=5):
	# returns the scaling factor and the list of each summation component for ln(M(DELTA,delta,G)/M(0))
	# D:= free diffusivity in m^2 s^-1
	# R:= cylinder radius in m
	# DELTA:= gradient separation in s
	# delta:= gradient width s
	# G:= gradient magnitude in T m^-1

	am_R = jnp_zeros(1,m_max)
	am = am_R / R
	am2 = am**2

	fac = -2*gamma**2*G**2/D**2

	comp = (1/(am**6*(am_R**2-1))) * (2*D*am2*delta - 2 + 2*np.exp(-D*am2*delta) + 2*np.exp(-D*am2*DELTA) - np.exp(-D*am2*(DELTA-delta)) - np.exp(-D*am2*(DELTA+delta)))

	return fac, comp


def vangelderen_cylinder_perp_ln_all(D, R, DELTA, delta, G, m_max=5):
	fac, comp = vangelderen_cylinder_perp_ln_list(D, R, DELTA, delta, G, m_max)
	return fac*np.cumsum(comp)

def vangelderen_cylinder_perp_ln(D, R, DELTA, delta, G, m_max=5):
	fac, comp = vangelderen_cylinder_perp_ln_list(D, R, DELTA, delta, G, m_max)
	return fac*np.sum(comp)

def vangelderen_cylinder_long_time_limit_ln(R, delta, G):
	return -0.25*(gamma*G*delta*R)**2
	# return (gamma*G*delta*R)**2 #?

def vangelderen_cylinder_short_time_limit_ln(D, DELTA, delta, G):
	return -(gamma*G*delta)**2 * (DELTA - delta/3.) * D

