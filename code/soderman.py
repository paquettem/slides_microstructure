import numpy as np
from scipy.special import jnp_zeros, jvp



#  T^-1 s^-1
gamma = 42.515e6 * 2*np.pi


def K(n,m):
	if (n==0) and (m==0):
		return 1.
	elif (n!=0) and (m!=0):
		return 4.
	else:
		return 2.





def soderman_cylinder_perp_list_q(D, R, DELTA, q, k_max=5, m_max=5):
	fac = 4*(2*np.pi*q*R)**2

	comp = []
	for k in range(1, k_max):
		alpha = jnp_zeros(k,m_max)
		# add alpha_k_0
		np.concatenate((np.ones(1), alpha), axis=0)
		S = 0
		for m in range(0, m_max):
			S += (K(0,m) * alpha[m]**2 * jvp(m,2*np.pi*q*R,1)**2 * np.exp(-(alpha[m]/R)**2 * D * DELTA)) / ((alpha[m]**2 - (2*np.pi*q*R)**2)**2 * (alpha[m]**2 - m**2))
		comp.append(S)

	return fac, np.array(comp)

def soderman_cylinder_perp_list(D, R, DELTA, delta, G, k_max=5, m_max=5):
	# returns the scaling factor and the list of each summation component for ln(M(DELTA,delta,G)/M(0))
	# D:= free diffusivity in m^2 s^-1
	# R:= cylinder radius in m
	# DELTA:= gradient separation in s
	# delta:= gradient width s
	# G:= gradient magnitude in T m^-1

	q = (gamma*G*delta)/(2*np.pi)

	return soderman_cylinder_perp_list_q(D, R, DELTA, q, k_max, m_max)


def soderman_cylinder_perp_all(D, R, DELTA, delta, G, k_max=5, m_max=5):
	fac, comp = soderman_cylinder_perp_list(D, R, DELTA, delta, G, k_max, m_max)
	return fac*np.cumsum(comp)


def soderman_cylinder_perp(D, R, DELTA, delta, G, k_max=5, m_max=5):
	fac, comp = soderman_cylinder_perp_list(D, R, DELTA, delta, G, k_max, m_max)
	return fac*np.sum(comp)






