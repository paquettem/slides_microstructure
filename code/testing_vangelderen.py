import pylab as pl
from vangelderen import *

#  T^-1 s^-1
gamma = 42.515e6 * 2*np.pi

# m^2/s
D = 1.5e-9

# m
R1 = 1e-6
R2 = 2.5e-6
R3 = 5e-6
R4 = 100e-6

# s
DELTAs = np.linspace(15e-3,100e-3,50)
deltas = np.linspace(1e-3,10e-3,10)

# T/m
G = 0.3

m_max = 10

signals = np.zeros((4, len(deltas), len(DELTAs), m_max))


for i_d, delta in enumerate(deltas):
	for i_D, DELTA in enumerate(DELTAs):

		signals[0, i_d, i_D] = vangelderen_cylinder_perp_ln_all(D, R1, DELTA, delta, G, m_max)
		signals[1, i_d, i_D] = vangelderen_cylinder_perp_ln_all(D, R2, DELTA, delta, G, m_max)
		signals[2, i_d, i_D] = vangelderen_cylinder_perp_ln_all(D, R3, DELTA, delta, G, m_max)
		signals[3, i_d, i_D] = vangelderen_cylinder_perp_ln_all(D, R4, DELTA, delta, G, m_max)




# for i_d, delta in enumerate(deltas):
# 	pl.figure()
# 	pl.subplot(1,3,1)
# 	for i in range(m_max):
# 		pl.plot(DELTAs, signals[0, i_d, :, i], label='{} terms'.format(i+1))
# 	pl.title('R1, delta={}'.format(deltas[i_d]))
# 	# pl.figure()
# 	pl.subplot(1,3,2)
# 	for i in range(m_max):
# 		pl.plot(DELTAs, signals[1, i_d, :, i], label='{} terms'.format(i+1))
# 	pl.title('R2, delta={}'.format(deltas[i_d]))
# 	# pl.figure()
# 	pl.subplot(1,3,3)
# 	for i in range(m_max):
# 		pl.plot(DELTAs, signals[2, i_d, :, i], label='{} terms'.format(i+1))
# 	pl.title('R3, delta={}'.format(deltas[i_d]))

# pl.show()



# for i_d, delta in enumerate(deltas):
# 	pl.figure()
# 	pl.subplot(1,3,1)
# 	for i in range(m_max):
# 		pl.plot(DELTAs, np.exp(signals[0, i_d, :, i]), label='{} terms'.format(i+1))
# 	pl.title('R1, delta={}'.format(deltas[i_d]))
# 	# pl.figure()
# 	pl.subplot(1,3,2)
# 	for i in range(m_max):
# 		pl.plot(DELTAs, np.exp(signals[1, i_d, :, i]), label='{} terms'.format(i+1))
# 	pl.title('R2, delta={}'.format(deltas[i_d]))
# 	# pl.figure()
# 	pl.subplot(1,3,3)
# 	for i in range(m_max):
# 		pl.plot(DELTAs, np.exp(signals[2, i_d, :, i]), label='{} terms'.format(i+1))
# 	pl.title('R3, delta={}'.format(deltas[i_d]))

# pl.show()


signal = np.zeros((4, len(deltas), len(DELTAs)))
signal_short_limit = np.zeros((len(deltas), len(DELTAs)))
signal_long_limit = np.zeros((4, len(deltas)))

for i_d, delta in enumerate(deltas):
	signal_long_limit[0, i_d] = vangelderen_cylinder_long_time_limit_ln(R1, delta, G)
	signal_long_limit[1, i_d] = vangelderen_cylinder_long_time_limit_ln(R2, delta, G)
	signal_long_limit[2, i_d] = vangelderen_cylinder_long_time_limit_ln(R3, delta, G)
	signal_long_limit[3, i_d] = vangelderen_cylinder_long_time_limit_ln(R4, delta, G)
	for i_D, DELTA in enumerate(DELTAs):

		signal[0, i_d, i_D] = vangelderen_cylinder_perp_ln(D, R1, DELTA, delta, G, m_max)
		signal[1, i_d, i_D] = vangelderen_cylinder_perp_ln(D, R2, DELTA, delta, G, m_max)
		signal[2, i_d, i_D] = vangelderen_cylinder_perp_ln(D, R3, DELTA, delta, G, m_max)
		signal[3, i_d, i_D] = vangelderen_cylinder_perp_ln(D, R4, DELTA, delta, G, m_max)

		signal_short_limit[i_d, i_D] = vangelderen_cylinder_short_time_limit_ln(D, DELTA, delta, G)



for i_d, delta in enumerate(deltas):
	pl.figure()
	c1 = (1,0,0)
	c2 = (0,1,0)
	c3 = (0,0,1)
	c5 = (0,0,0)
	c4 = (1,0,1)
	pl.plot(DELTAs, np.exp(signal[0, i_d]), label='R1 = {}'.format(R1), c=c1)
	pl.plot(DELTAs, np.exp(signal[1, i_d]), label='R2 = {}'.format(R2), c=c2)
	pl.plot(DELTAs, np.exp(signal[2, i_d]), label='R3 = {}'.format(R3), c=c3)
	pl.plot(DELTAs, np.exp(signal[3, i_d]), label='R4 = {}'.format(R4), c=c5)
	pl.axhline(np.exp(signal_long_limit[0, i_d]), label='long R1', c=c1)
	pl.axhline(np.exp(signal_long_limit[1, i_d]), label='long R2', c=c2)
	pl.axhline(np.exp(signal_long_limit[2, i_d]), label='long R3', c=c3)
	pl.axhline(np.exp(signal_long_limit[3, i_d]), label='long R4', c=c5)
	pl.plot(DELTAs, np.exp(signal_short_limit[i_d]), label='Free', c=c4)
	pl.title('deltas = {:.1e} s, bval = [ {:.1f}  {:.1f} ] s/mm^2'.format(delta, (1e-6)*(gamma*G*delta)**2*(DELTAs[0]-delta/3.), (1e-6)*(gamma*G*delta)**2*(DELTAs[-1]-delta/3.)))
	pl.legend()
pl.show()


